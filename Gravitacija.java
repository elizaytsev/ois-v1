import java.util.Scanner;

public class Gravitacija {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        double seaLevelHeights = sc.nextDouble();
        double M = 5.972 * 1000000000000000000000000.0;     //Earth mass
        double r = 6.371 * 1000000;                       //Earth radius
        double C = 6.674 * 0.00000000001;
        heightAndAcceleration(seaLevelHeights, accelerationCalculate(M, C, r, seaLevelHeights));
    }
    private static void  heightAndAcceleration(double height, double acc){       //Dejan
        System.out.println("Height: " + height);
        System.out.println("Acceleration: " + acc);
    }
    private static double accelerationCalculate(double M, double C, double r, double v){//Hana
     double acceleration= (C*M)/((r+v)*(r+v));
        return acceleration;
    }
    private static void accelerationPrint(double M, double C, double r, double v){           //Ilya
        System.out.println(accelerationCalculate(M, C, r, v));
    }
}